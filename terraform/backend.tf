terraform {
  backend "s3" {
    bucket = "terraform.johnlarsen.name"
    key    = "backup-monitor"
    region = "ap-southeast-2"
  }
}
