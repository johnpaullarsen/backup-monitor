variable "common_tags" {
  type        = map
  description = "These tags will be appended to all shared resources created by terraform"
  default = {
    "category"        = "infrastructure"
    "provisioner"     = "terraform"
  }
}

