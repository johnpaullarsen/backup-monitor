locals {
  project_bucket = "lambdaprojects-johnlarsen.name"
  s3_key         = "backup-monitor/v1.0.0/backup-monitor.zip"
  rclone_s3_key  = "backup-monitor/v1.0.0/rclone.zip"
}

# IAM role which dictates what other AWS services the Lambda function may access.
resource "aws_iam_role" "backup_monitor_lambda_role" {
  name = "backup_monitor_lambda_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "backup_monitor_lambda_policy" {
  name   = "backup-monitor-lambda-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": [
        "ssm:GetParameter",
        "ssm:GetParametersByPath"
      ],
      "Resource": [ "arn:aws:ssm:ap-southeast-2:679890246078:parameter/app/backup-monitor/*" ]
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": [
        "sqs:SendMessage"
      ],
      "Resource": [ "arn:aws:sqs:ap-southeast-2:679890246078:event-receiver-queue" ]
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Action": [
        "cloudwatch:PutMetricData"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_lambda_basic_to_role" {
  role       = aws_iam_role.backup_monitor_lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "attach_backup_monitor_policy_to_role" {
  role       = aws_iam_role.backup_monitor_lambda_role.name
  policy_arn = aws_iam_policy.backup_monitor_lambda_policy.arn
}

resource "aws_cloudwatch_event_rule" "backup_monitor_johnl" {
  name        = "backup-monitor-johnl"
  description = "Run backup monitor for John"
  # Every hour on the hour
  schedule_expression = "cron(0 * * * ? *)"
}

# Send events matching backup_monitor_johnl to backup monitor lambda
resource "aws_cloudwatch_event_target" "backup_monitor_johnl_rule_to_backup_monitor_lambda" {
  target_id = "backup-monitor-johnl-target"
  rule = aws_cloudwatch_event_rule.backup_monitor_johnl.name
  arn = aws_lambda_function.backup_monitor.arn
  input_transformer {
    input_template = <<EOF
{
  "user": "johnl",
  "computer": "pablo",
  "storage": "onedrive"
}
EOF
  }
}

resource "aws_cloudwatch_event_rule" "backup_monitor_sharonp" {
  name        = "backup-monitor-sharonp"
  description = "Run backup monitor for Sharon"
  # Every hour on the hour
  schedule_expression = "cron(0 * * * ? *)"
}

# Send events matching backup_monitor_sharonp to backup monitor lambda
resource "aws_cloudwatch_event_target" "backup_monitor_sharonp_rule_to_backup_monitor_lambda" {
  target_id = "backup-monitor-sharonp-target"
  rule = aws_cloudwatch_event_rule.backup_monitor_sharonp.name
  arn = aws_lambda_function.backup_monitor.arn
  input_transformer {
    input_template = <<EOF
{
  "user": "sharonp",
  "computer": "pablo",
  "storage": "onedrive"
}
EOF
  }
}

# Allow event bridge backup monitor rule to call the backup monitor lambda
resource "aws_lambda_permission" "event_bridge_invoke_backup_monitor" {
  statement_id = "event-bridge-invoke-backup-monitor"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.backup_monitor.function_name
  principal = "events.amazonaws.com"
  source_arn = "arn:aws:events:ap-southeast-2:679890246078:rule/*"
}

resource "aws_lambda_layer_version" "rclone_layer" {
  layer_name       = "rclone_layer"
  s3_bucket        = local.project_bucket
  s3_key           = local.rclone_s3_key
  source_code_hash = filebase64sha256("../bin/rclone.zip")
  compatible_runtimes = ["python3.7", "python3.8"]
}

resource "aws_lambda_function" "backup_monitor" {
  function_name = "BackupMonitor"
  s3_bucket        = local.project_bucket
  s3_key           = local.s3_key
  source_code_hash = filebase64sha256("../build/backup-monitor.zip")
  timeout          = 180
  layers           = [aws_lambda_layer_version.rclone_layer.arn]

  handler = "backup_monitor_lambda.handler"
  runtime = "python3.7"
  role = aws_iam_role.backup_monitor_lambda_role.arn
}
