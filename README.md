# backup-monitor
Monitor cloud sync, backup and restore using cloudberry, S3 and cloudwatch


### Requirements

[boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)

[rclone](https://rclone.org/) 


### Refreshing OneDrive Token

Need a host that has rclone and web-browser available (mac?).

1. On the mac run: `rclone authorize "onedrive"`
1. Log into onedrive using johnl or sharonp as appropriate. The access token will be printed to the rclone console 
2. Copy the access token json object into SSM parameters in AWS under the `/app/backup-monitor/pablo/onedrive/johnl/token` path.
3. Repeat all steps for the sharonp user using sharons onedrive account.

### Docs

[Serverless Application Model](https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md)

[OneDrive Python API](https://github.com/OneDrive/onedrive-sdk-python)

`pip install https://github.com/OneDrive/onedrive-sdk-python/archive/master.zip`