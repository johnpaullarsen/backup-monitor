#!/bin/bash

LAMBDA_PROJECTS_BUCKET_BASE="s3://lambdaprojects-johnlarsen.name/backup-monitor/v1.0.0"

mkdir -p build

# Create zipped version of rclone for creation of lambda layer
# zip -j bin/rclone.zip bin/rclone
#aws s3 cp bin/rclone.zip "${LAMBDA_PROJECTS_BUCKET_BASE}/rclone.zip"

zip -j build/backup-monitor.zip src/backup_monitor/backup_monitor_lambda.py src/backup_monitor/backup_monitor.py
aws s3 cp build/backup-monitor.zip "${LAMBDA_PROJECTS_BUCKET_BASE}/backup-monitor.zip"

if test -f "/usr/local/terraform13/bin/terraform"; then
  TERRAFORM="/usr/local/terraform13/bin/terraform"
else
  TERRAFORM="terraform"
fi

cd terraform
$TERRAFORM apply -auto-approve
cd -
