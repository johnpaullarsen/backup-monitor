#!/bin/bash

set -euxo pipefail

USER=johnl
COMPUTER=pablo
BACKUP_MONITOR_PATH="/home/${USER}/onedrive-${USER}/backup-monitor/${COMPUTER}/onedrive-${USER}"

# Restore the generated canary to the restored directory
/usr/bin/kopia snapshot restore --snapshot-time=latest "${BACKUP_MONITOR_PATH}/generated/canary.json" "${BACKUP_MONITOR_PATH}/restored/canary.json"

# Calculate the size of the backup and output to restored directory
$HOME/bin/rclone size /home/${USER}/onedrive-${USER} --json > "${BACKUP_MONITOR_PATH}/restored/size.json"

# Copy the restored files back to onedrive
$HOME/bin/rclone copy -I "${BACKUP_MONITOR_PATH}/restored/canary.json" "onedrive-${USER}:/backup-monitor/${COMPUTER}/onedrive-${USER}/restored"
$HOME/bin/rclone copy -I "${BACKUP_MONITOR_PATH}/restored/size.json" "onedrive-${USER}:/backup-monitor/${COMPUTER}/onedrive-${USER}/restored"
