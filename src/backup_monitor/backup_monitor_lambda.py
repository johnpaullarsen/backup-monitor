import tempfile
import boto3
import logging

from backup_monitor import BackupMonitor


def configure_aws_logging():
    """
    Log to stdout at INFO level seems to be the best bet for logging lambdas to cloudwatch
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    

def create_rclone_config(computer, storage, user):
    """
    Get the token and drive id from ssm parameters, then write an rclone config file to a temp location
    :return: The config file object, which should be closed when the client is done with it.
    """
    parameter_path = f'/app/backup-monitor/{computer}/{storage}/{user}'
    client = boto3.client('ssm')
    response = client.get_parameters_by_path(Path=parameter_path, WithDecryption=True)
    # Strip the parameter path off the name. Create params dict using dict comprehension
    params = {p['Name'][len(parameter_path) + 1:]: p['Value'] for p in response['Parameters']}
    token = params['token']
    drive_id = params['drive-id']
    fp = tempfile.NamedTemporaryFile(mode="w")
    fp.write(f"[{storage}-{user}]\n")
    fp.write(f"type = {storage}\n")
    fp.write(f"token = {token}\n")
    fp.write(f"drive_id = {drive_id}\n")
    fp.write('drive_type = personal\n')
    fp.flush()
    return fp


def handler(event, context):
    """
    Lambda event handler. Expects event to be a dict containing 'computer', 'storage' and 'user'
    """
    logging.info(event)
    computer = event['computer']
    storage = event['storage']
    user = event['user']

    with create_rclone_config(computer, storage, user) as rclone_config_file:
        backup_monitor = BackupMonitor(computer, storage, user, '/opt/rclone', rclone_config_file.name)
        backup_monitor.monitor()


configure_aws_logging()
